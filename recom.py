critics={'Lisa Rose': {'Lady in the Water': 2.5, 'Snakes on a Plane': 3.5,'Just My Luck': 3.0, 'Superman Returns': 3.5, 'You, Me and Dupree': 2.5,'The Night Listener': 3.0},
'Gene Seymour': {'Lady in the Water': 3.0, 'Snakes on a Plane': 3.5,'Just My Luck': 1.5, 'Superman Returns': 5.0, 'The Night Listener': 3.0,'You, Me and Dupree': 3.5},
'Michael Phillips': {'Lady in the Water': 2.5, 'Snakes on a Plane': 3.0,'Superman Returns': 3.5, 'The Night Listener': 4.0},
'Claudia Puig': {'Snakes on a Plane': 3.5, 'Just My Luck': 3.0,'The Night Listener': 4.5, 'Superman Returns': 4.0,'You, Me and Dupree': 2.5},
'Mick LaSalle': {'Lady in the Water': 3.0, 'Snakes on a Plane': 4.0,'Just My Luck': 2.0, 'Superman Returns': 3.0, 'The Night Listener': 3.0,'You, Me and Dupree': 2.0},
'Jack Matthews': {'Lady in the Water': 3.0, 'Snakes on a Plane': 4.0,'The Night Listener': 3.0, 'Superman Returns': 5.0, 'You, Me and Dupree': 3.5},
'Toby': {'Snakes on a Plane':4.5,'You, Me and Dupree':1.0,'Superman Returns':4.0},"me":{'Superman Returns': 5.0, 'You, Me and Dupree': 4.5,'The Night Listener': 3.5}}


import math
def sim_distance(prefs, per1,per2):
    si = {}
    for item in prefs[per1]:
        if item in prefs[per2]:
            si[item] = 1
    if len(si)==0:
        return 0
    sum_of_scv = sum([pow(prefs[per1][item]-prefs[per2][item],2)for item in prefs[per1] if item in prefs[per2]])
    return 1/(1+sum_of_scv)
def sim_pearson(pref,p1,p2):
    si = {}
    for item in pref[p1]:
        if item in pref[p2]:
            si[item] = 1
    n = len(si)
    if n == 0:
        return 0
    sum1 = sum([pref[p1][item] for item in si])
    sum2 = sum([pref[p2][item] for item in si])
    sum1Sq = sum([pow(pref[p1][item],2) for item in si])
    sum2Sq = sum([pow(pref[p2][item],2) for item in si])
    pSum = sum([pref[p1][item]* pref[p2][item] for item in si])
    num = pSum-(sum1*sum2/n)
    den = pow((sum1Sq - pow(sum1,2)/n)*(sum2Sq-pow(sum2,2)/n),0.5)
    if den == 0:
        return 0
    r = num/den
    return r
def topMatches(prefs,person,n = 4,similarity = sim_pearson):
    scores=[(similarity(prefs,person,other),other)for other in prefs if other != person]
    scores.sort()
    scores.reverse()
    return scores[0:n]

def getRecommend(prefs,person,similarity = sim_pearson):
    totals = {}
    simSums = {}
    for other in prefs:
        if other == person: continue
        sim=similarity(prefs,person,other)
        if sim <=0:continue
        for item in prefs[other]:
            if item not in prefs[person] or prefs[person][item] ==0:
                totals.setdefault(item,0)
                totals[item] +=prefs[other][item]*sim
                simSums.setdefault(item,0)
                simSums[item]+=sim
    rankings =[(total/simSums[item],item)for item,total in totals.items()]
    rankings.sort()
    rankings.reverse()
    return rankings
    





    
    
